#!/bin/sh

# JamfSplash is a splash screen to help prevent users from interacting with the Mac while management tasks are completing.
# This script is intended to run at the beginning of a Jamf Pro policy.
# Make sure this script is set to run "Before" the rest of the policy.
# The splash screen will not quit automatically, as it's assumed that your policy will eventually logout or restart.
# Kevin M. White - v1.3 - 6/6/18

# Change your organization variables here:
screenNAME="Macjutsu" # Organization name shown on the splash screen.
screenIMAGE="https://d3grfap2l5ikgv.cloudfront.net/59a57de8ca753d311f7444bc.png" # Image shown on the splash screen. You can specify a path to an image file built in to macOS or a publicly accesable URL.
sayNAME="Mac jutsu" # Organization name spoken outloud. It's best to be informal here. Also, you may have to try different spellings to get the speech engine working correctly.
sayVOICE="Samantha" # In Terminal use "say -v ?" to see the full list of speech options.

# If the screen image is a URL, check to make sure it's available and then download the file.
if [[ "$screenIMAGE" =~ http* ]]; then
  if [[ $(curl -s --head -w %{http_code} $screenIMAGE -o /dev/null) == 200 ]]; then
    echo "JamfSplash: Downloading screen image."
    curl -o /tmp/JamfSplash "$screenIMAGE" 2>/dev/null
    screenIMAGE="/tmp/JamfSplash"
  else
    echo "JamfSplash: The requested screen image cannot be downloaded."
  fi
fi

# Verify that screen image exists, if not use the default Finder icon.
if [[ -f "$screenIMAGE" ]]; then
  echo "JamfSplash: Screen image found."
else
  echo "JamfSplash: Image missing, using default Finder icon."
  screenIMAGE="/System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/FinderIcon.icns"
fi

# Set the system audio volume. Change digit at end (1-10).
osascript -e "set Volume 8"

# Wait for Dock to open, this is a good indication the user is logged in and Setup Assistant is complete.
while ! pgrep -x "Dock" > /dev/null; do
    sleep 1
done

# Get the current user's account name.
currentUSER=$(/usr/bin/python -c 'from SystemConfiguration import SCDynamicStoreCopyConsoleUser; import sys; username = (SCDynamicStoreCopyConsoleUser(None, None, None) or [None])[0]; username = [username,""][username in [u"loginwindow", None, u""]]; sys.stdout.write(username + "\n");')

# Another wait to ensure the login process is complete.
while [[ "$currentUSER" == "loginwindow" || "$currentUSER" == "_mbsetupuser" || "$currentUSER" == "root" ]]; do
  echo "Current user still in login limbo: $currentUSER"
  sleep 1
  currentUSER=$(/usr/bin/python -c 'from SystemConfiguration import SCDynamicStoreCopyConsoleUser; import sys; username = (SCDynamicStoreCopyConsoleUser(None, None, None) or [None])[0]; username = [username,""][username in [u"loginwindow", None, u""]]; sys.stdout.write(username + "\n");')
done

# Get the current user's First Name.
firstNAME=`dscl . -read /Users/$currentUSER RealName | tail -1 | awk '{ print $1}'`

# Fix first name if the RealName attribute is empty.
if [[ "$firstNAME" == "RealName:" ]]; then
    firstNAME="$currentUSER"
fi

# JamfSplash screen.
# Edit the quoted text to modify the screen text.
# Note that a full return will display a new line. Just make sure to complete the message with a quote at the end.
sudo -u $currentUSER /Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType fs -heading "$screenNAME Computer Setup" -icon "$screenIMAGE" -description "Hello $firstNAME! Your $screenNAME computer will be ready in a few minutes.
When this setup is done, the computer will automatically restart and be ready for you to use." &
echo "JamfSplash: Showing splash screen for $currentUSER. - `date`"
sleep 1

# JamfSplash speech.
# Edit the quoted text to modify the spoken text, or comment out the line to prevent the speech entirely.
say -v $sayVOICE "Hello $firstNAME! Your $sayNAME computer will be ready in a few minutes. When this setup is done, the computer will automatically restart and be ready for you to use." &

# Open the Jamf log in case you quit the JamfSplash screen (via Command-Q).
sudo -u $currentUSER open /var/log/jamf.log &
