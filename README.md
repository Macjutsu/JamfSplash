# JamfSplash

A lightweight splash screen for [Jamf Pro](https://www.jamf.com/products/jamf-pro/) deployments.

## Getting Started

This is a script to be run by [Jamf Pro](https://www.jamf.com/products/jamf-pro/) that will present a full screen splash to discourage user interaction.

### Prerequisites

This script requires macOS with jamfhelper installed in the default location.

Typically this would be a Mac computer that is enrolled in [Jamf Pro](https://www.jamf.com/products/jamf-pro/).

### Installing and Testing

JamfSplash can be installed and run from any path on the computer. It can also run at any time simply by running the script in Terminal. However, because the jamf agent runs all scripts as root, you should also test run JamfSplash as root.

```
sudo /path/to/00.JamfSplash.sh
```

IMPORTANT: You must manually quit the splash screen via keyboard shortcut (Command-Q)!

![alt text](Screenshot.png "JamfSplash Screenshot")

## Jamf Pro Deployment

You can set a Jamf Pro Policy to run JamfSplash anytime a user is logged into the Mac (via the GUI). The most typical use case is to run JamfSplash as soon as possible after enrollment completes.

### Jamf Pro Recommendations

On your Jamf Pro server:

* Configure the script name so it is alphabetically first, "00.JamfSplash.sh"
* Configure the script to run "Before" installers.
* Name a Policy so it is alphabetically first, "00.Setup".
* Set the Policy to Trigger on "Enrollment Complete" and Execution Frequency of "Once Per Computer".
* Other polices that also use the "Enrollment Complete" Trigger should run after the splash screen is up.

### JamfSplash Cleanup

JamfSplash will background the splash screen, thus it will stay open until the jamfhelper process is killed or the computer restarts. The most common method to resolve this is to set a final Jamf Pro Policy to restart the computer once all other policies have completed.

Also, JamfSplash will automatically open the jamf.log (via Console.app) so you can view progress if you choose to quit the splash screen (Command-Q). As a default, macOS will automatically re-open Console.app when the user logs back into the Mac.

Given these cleanup recommendations, on Jamf Pro:

* Name a Policy so it is alphabetically last, "ZZ.SetupCompete".
* Set the Policy to Trigger on "Enrollment Complete" and Execution Frequency of "Once Per Computer".
* In "Files and Processes" enter "Console" in "Search For Process" and then select "Kill process if found".
* In "Restart Options" select "Restart immediately".
* For good measure, in "Maintenance" select "Update Inventory".

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
